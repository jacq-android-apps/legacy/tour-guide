package com.example.android.tourguide;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class NightLifeFragment extends Fragment {


    public NightLifeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.list_view, container, false);

        ArrayList<FoodDrink_Details> foodDrinkDetails = new ArrayList<FoodDrink_Details>();
        foodDrinkDetails.add(new FoodDrink_Details(R.drawable.womb1, R.drawable.womb2,R.drawable.womb3,
                R.drawable.womb4, R.drawable.womb5, R.drawable.ghibli_review,
                getString(R.string.night1),getString(R.string.womb)));

        foodDrinkDetails.add(new FoodDrink_Details(R.drawable.odeon1, R.drawable.odeon2,R.drawable.odeon3,
                R.drawable.odeon4, R.drawable.odeon5, R.drawable.odeon_review,
                getString(R.string.night2),getString(R.string.odeon)));


        FoodDrinkAdapter itemsAdapter = new FoodDrinkAdapter(getActivity(), foodDrinkDetails);
        ListView view = (ListView) v.findViewById(R.id.list);
        view.setAdapter(itemsAdapter);

        TextView Nav_Title = (TextView) v.findViewById(R.id.guideTitle);
        Nav_Title.setText(getString(R.string.nightTitle));
        Typeface title = Typeface.createFromAsset(getContext().getAssets(), getString(R.string.fonts));
        Nav_Title.setTypeface(title);


        return v;
    }

}
