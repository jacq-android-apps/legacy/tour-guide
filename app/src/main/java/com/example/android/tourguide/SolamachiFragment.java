package com.example.android.tourguide;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class SolamachiFragment extends Fragment {


    public SolamachiFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_solamachi, container, false);

        TextView myTextView7 = (TextView) v.findViewById(R.id.shop1);
        Typeface typeface7 = Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.fonts));
        myTextView7.setTypeface(typeface7);

        return v;
    }

}
