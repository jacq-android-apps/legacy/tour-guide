package com.example.android.tourguide;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class FoodDrinksFragment extends Fragment {


    public FoodDrinksFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.list_view, container, false);


        ArrayList<FoodDrink_Details> foodDrinkDetails = new ArrayList<FoodDrink_Details>();
        foodDrinkDetails.add(new FoodDrink_Details(R.drawable.narisawa1, R.drawable.narisawa2,R.drawable.narisawa3,
                R.drawable.narisawa4, R.drawable.narisawa5, R.drawable.narisawa_review,
                getString(R.string.food1),getString(R.string.narisawa)));

        foodDrinkDetails.add(new FoodDrink_Details(R.drawable.ichiran1, R.drawable.ichiran2,R.drawable.ichiran3,
                R.drawable.ichiran4, R.drawable.ichiran5, R.drawable.ichiran_review,
                getString(R.string.food2),getString(R.string.ichiran)));

        foodDrinkDetails.add(new FoodDrink_Details(R.drawable.ippudo1, R.drawable.ippudo2,R.drawable.ippudo3,
                R.drawable.ippudo4, R.drawable.ippudo5, R.drawable.ippudo_review,
                getString(R.string.food3),getString(R.string.ippudo)));

        FoodDrinkAdapter itemsAdapter = new FoodDrinkAdapter(getActivity(), foodDrinkDetails);
        ListView view = (ListView) v.findViewById(R.id.list);
        view.setAdapter(itemsAdapter);

        TextView Nav_Title = (TextView) v.findViewById(R.id.guideTitle);
        Nav_Title.setText(getString(R.string.foodTitle));
        Typeface title = Typeface.createFromAsset(getContext().getAssets(), getString(R.string.fonts));
        Nav_Title.setTypeface(title);

        return v;
    }

}
