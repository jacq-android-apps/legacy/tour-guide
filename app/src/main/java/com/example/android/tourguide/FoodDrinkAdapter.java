package com.example.android.tourguide;
import android.app.Activity;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
/**
 * Created by Jacquelyn Gboyor on 10/8/2017.
 */

public class FoodDrinkAdapter extends ArrayAdapter<FoodDrink_Details>{

    public FoodDrinkAdapter(Activity context, ArrayList<FoodDrink_Details> foodDrinkDetails) {
        // Here, we initialize the ArrayAdapter's internal storage for the context and the list.
        // the second argument is used when the ArrayAdapter is populating a single TextView.
        // Because this is a custom adapter for two TextViews and an ImageView, the adapter is not
        // going to use this second argument, so it can be any value. Here, we used 0.
        super(context, 0, foodDrinkDetails);
    }

    String font = "fonts/Prestij_Light_Demo.otf";
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItemView = convertView;
        if(listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_items_fooddrinks, parent, false);
        }

        FoodDrink_Details currentGuide = getItem(position);
        Typeface typeface3 = Typeface.createFromAsset(getContext().getAssets(), font);

        ImageView picture1 = (ImageView) listItemView.findViewById(R.id.pic1);
        picture1.setImageResource(currentGuide.getPicture1());

        ImageView picture2 = (ImageView) listItemView.findViewById(R.id.pic2);
        picture2.setImageResource(currentGuide.getPicture2());

        ImageView picture3 = (ImageView) listItemView.findViewById(R.id.pic3);
        picture3.setImageResource(currentGuide.getPicture3());

        ImageView picture4 = (ImageView) listItemView.findViewById(R.id.pic4);
        picture4.setImageResource(currentGuide.getPicture4());

        ImageView picture5 = (ImageView) listItemView.findViewById(R.id.pic5);
        picture5.setImageResource(currentGuide.getPicture5());

        ImageView rev = (ImageView) listItemView.findViewById(R.id.reviews);
        rev.setImageResource(currentGuide.getReview());

        TextView name = (TextView) listItemView.findViewById(R.id.name);
        name.setText(currentGuide.getName());
        name.setTypeface(typeface3);

        TextView info = (TextView) listItemView.findViewById(R.id.information);
        info.setText(currentGuide.getInformation());

        return listItemView;
    }
}
