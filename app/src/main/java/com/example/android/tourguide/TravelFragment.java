package com.example.android.tourguide;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class TravelFragment extends Fragment {


    public TravelFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_travels, container, false);

        ImageButton travel = (ImageButton) v.findViewById(R.id.clickTravel1);
        travel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.travelInfo, new StationFragment()).addToBackStack(null).commit();
            }
        });

        TextView myTextView7 = (TextView) v.findViewById(R.id.travel1);
        Typeface typeface7 = Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.fonts));
        myTextView7.setTypeface(typeface7);

        TextView myTextView1 = (TextView) v.findViewById(R.id.travel_choice);
        myTextView1.setTypeface(typeface7);

        TextView myTextView = (TextView) v.findViewById(R.id.travel_name);
        myTextView.setTypeface(typeface7);

        return v;
    }

}
