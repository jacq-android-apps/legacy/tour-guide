package com.example.android.tourguide;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class ShoppingFragment extends Fragment {


    public ShoppingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_shopping, container, false);

        ImageButton shop = (ImageButton) v.findViewById(R.id.clickShop1);
        shop.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.shopInfo, new KitteFragment()).addToBackStack(null).commit();
            }
        });

        ImageButton shop2 = (ImageButton) v.findViewById(R.id.clickShop2);
        shop2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.shopInfo, new SolamachiFragment()).addToBackStack(null).commit();
            }
        });
        TextView myTextView = (TextView) v.findViewById(R.id.shop_name);
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.fonts));
        myTextView.setTypeface(typeface);

        TextView myTextView7 = (TextView) v.findViewById(R.id.shop_choice);
        myTextView7.setTypeface(typeface);

        TextView myTextView1 = (TextView) v.findViewById(R.id.shop1);
        myTextView1.setTypeface(typeface);

        TextView myTextView2 = (TextView) v.findViewById(R.id.shop2);
        myTextView2.setTypeface(typeface);

        return v;
    }

}
