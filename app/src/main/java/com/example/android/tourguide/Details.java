package com.example.android.tourguide;

import android.widget.ImageButton;

/**
 * Created by Jacquelyn Gboyor on 10/6/2017.
 */

public class Details {

    private int picture1; private int picture2; private int reviews;
    private String location_name; private String paragraph;
    private int amenity1; private String amenity1name;
    private int amenity2; private String amenity2name;
    private int amenity3; private String amenity3name;
    private int amenity4; private String amenity4name;
    private int amenity5; private String amenity5name;
    private int amenity6; private String amenity6name;
    private int amenity7; private String amenity7name;
    private int amenity8; private String amenity8name;
    private int amenity9; private String amenity9name;

    public Details(int pic1, int pic2, int review, String location, String paragraphs,
                   int amenities1, String amenities1name, int amenities2, String amenities2name,
                   int amenities3, String amenities3name, int amenities4, String amenities4name,
                   int amenities5, String amenities5name, int amenities6, String amenities6name,
                   int amenities7, String amenities7name, int amenities8, String amenities8name,
                   int amenities9, String amenities9name){

        picture1 = pic1; picture2 = pic2;
        reviews = review;
        location_name = location;
        paragraph = paragraphs;
        amenity1 = amenities1; amenity1name = amenities1name;
        amenity2 = amenities2; amenity2name = amenities2name;
        amenity3 = amenities3; amenity3name = amenities3name;
        amenity4 = amenities4; amenity4name = amenities4name;
        amenity5 = amenities5; amenity5name = amenities5name;
        amenity6 = amenities6; amenity6name = amenities6name;
        amenity7 = amenities7; amenity7name = amenities7name;
        amenity8 = amenities8; amenity8name = amenities8name;
        amenity9 = amenities9; amenity9name = amenities9name;

    }//Details

    public int getPicture1(){
        return picture1;
    }
    public int getPicture2 (){
        return picture2;
    }
    public int getReviews(){
        return reviews;
    }
    public String getLocation_name(){
        return location_name;
    }
    public String getParagraph(){
        return paragraph;
    }
    public int getAmenity1(){
        return amenity1;
    }
    public String getAmenity1name(){
        return amenity1name;
    }
    public int getAmenity2(){
        return amenity2;
    }
    public String getAmenity2name(){
        return amenity2name;
    }
    public int getAmenity3(){
        return amenity3;
    }
    public String getAmenity3name(){
        return amenity3name;
    }
    public int getAmenity4(){
        return amenity4;
    }
    public String getAmenity4name(){
        return amenity4name;
    }
    public int getAmenity5() {
        return amenity5;
    }
    public String getAmenity5name() {
        return amenity5name;
    }
    public int getAmenity6() {
        return amenity6;
    }
    public String getAmenity6name(){
        return amenity6name;
    }
    public int getAmenity7(){
        return amenity7;
    }
    public String getAmenity7name(){
        return amenity7name;
    }
    public int getAmenity8(){
        return amenity8;
    }
    public String getAmenity8name(){
        return amenity8name;
    }
    public int getAmenity9(){
        return amenity9;
    }
    public String getAmenity9name(){
        return amenity9name;
    }

}//End class
