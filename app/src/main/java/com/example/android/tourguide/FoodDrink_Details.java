package com.example.android.tourguide;

/**
 * Created by Jacquelyn Gboyor on 10/8/2017.
 */

public class FoodDrink_Details {

    private int picture1;
    private int picture2;
    private int picture3;
    private int picture4;
    private int picture5;
    private int review;
    private String name;
    private String information;

    public FoodDrink_Details(int pic1, int pic2, int pic3, int pic4,
                             int pic5, int reviews, String names, String info){

        picture1 = pic1;
        picture2 = pic2;
        picture3 = pic3;
        picture4 = pic4;
        picture5 = pic5;
        review = reviews;
        name = names;
        information = info;
    }

    public int getPicture1(){
        return picture1;
    }
    public int getPicture2(){
        return picture2;
    }
    public int getPicture3(){
        return picture3;
    }
    public int getPicture4(){
        return picture4;
    }
    public int getPicture5(){
        return picture5;
    }

    public int getReview() {
        return review;
    }
    public String getName(){
        return name;
    }
    public String getInformation(){
        return information;
    }
}//class
