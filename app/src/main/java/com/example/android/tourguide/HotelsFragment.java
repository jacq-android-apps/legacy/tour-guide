package com.example.android.tourguide;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class HotelsFragment extends Fragment {


    public HotelsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.list_view, container, false);

        ArrayList<Details> detail = new ArrayList<Details>();
        detail.add(new Details(R.drawable.hyatt1, R.drawable.hyatt2, R.drawable.hyatt_review,
                getString(R.string.hotel2), getString(R.string.hyatt), R.drawable.wifi,
                getString(R.string.wifi), R.drawable.pool, getString(R.string.pool),
                R.drawable.restaurant, getString(R.string.restaurant), R.drawable.not,
                getString(R.string.noBreakfast), R.drawable.bar, getString(R.string.bar),
                R.drawable.parking, getString(R.string.parking), R.drawable.laundry,
                getString(R.string.laundry), R.drawable.hot_tub, getString(R.string.hottub),
                R.drawable.spa, getString(R.string.spa)));

        detail.add(new Details(R.drawable.conrad1, R.drawable.conrad2, R.drawable.conrand_review,
                getString(R.string.hotel1), getString(R.string.conrad), R.drawable.wifi,
                getString(R.string.wifi), R.drawable.pool, getString(R.string.pool),
                R.drawable.restaurant, getString(R.string.restaurant), R.drawable.not,
                getString(R.string.noBreakfast), R.drawable.bar, getString(R.string.bar),
                R.drawable.parking, getString(R.string.parking), R.drawable.laundry,
                getString(R.string.laundry), R.drawable.hot_tub, getString(R.string.hottub),
                R.drawable.spa, getString(R.string.spa)));

        detail.add(new Details(R.drawable.mandarin1, R.drawable.mandarin2, R.drawable.mandarin_review,
                getString(R.string.hotel3), getString(R.string.mandarin), R.drawable.wifi,
                getString(R.string.wifi), R.drawable.pool, getString(R.string.pool),
                R.drawable.restaurant, getString(R.string.restaurant), R.drawable.not,
                getString(R.string.noBreakfast), R.drawable.bar, getString(R.string.bar),
                R.drawable.parking, getString(R.string.parking), R.drawable.laundry,
                getString(R.string.laundry), R.drawable.hot_tub, getString(R.string.hottub),
                R.drawable.spa, getString(R.string.spa)));


        DetailsAdapter itemsAdapter = new DetailsAdapter(getActivity(), detail);
        ListView view = (ListView) v.findViewById(R.id.list);
        view.setAdapter(itemsAdapter);

        TextView Nav_Title = (TextView) v.findViewById(R.id.guideTitle);
        Nav_Title.setText(getString(R.string.hotelTitle));
        Typeface title = Typeface.createFromAsset(getContext().getAssets(), getString(R.string.fonts));
        Nav_Title.setTypeface(title);

        return v;
    }//Main


}//Class
