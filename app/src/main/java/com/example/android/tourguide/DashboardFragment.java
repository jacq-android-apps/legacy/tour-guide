package com.example.android.tourguide;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends Fragment {

    //int fonts = R.string.fonts;

    public DashboardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_dashboard, container, false);


        TextView myTextView = (TextView) v.findViewById(R.id.tour1);
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.fonts));
        myTextView.setTypeface(typeface);

        TextView myTextView2 = (TextView) v.findViewById(R.id.tour2);
        myTextView2.setTypeface(typeface);

        TextView myTextView3 = (TextView) v.findViewById(R.id.tour3);
        myTextView3.setTypeface(typeface);

        TextView myTextView4 = (TextView) v.findViewById(R.id.tour4);
        myTextView4.setTypeface(typeface);

        TextView myTextView5 = (TextView) v.findViewById(R.id.tour5);
        myTextView5.setTypeface(typeface);

        TextView myTextView6 = (TextView) v.findViewById(R.id.tour6);
        myTextView6.setTypeface(typeface);

        TextView myTextView7 = (TextView) v.findViewById(R.id.tour7);
        myTextView7.setTypeface(typeface);


        ImageButton hotel = (ImageButton) v.findViewById(R.id.hotels);
        hotel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new HotelsFragment()).addToBackStack(null).commit();
                }
            });

        ImageButton fooddrink = (ImageButton) v.findViewById(R.id.fooddrink);
        fooddrink.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new FoodDrinksFragment()).addToBackStack(null).commit();
            }
        });

        ImageButton museum = (ImageButton) v.findViewById(R.id.museum);
        museum.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new MuseumsFragment()).addToBackStack(null).commit();
            }
        });

        ImageButton night = (ImageButton) v.findViewById(R.id.nightlife);
        night.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new NightLifeFragment()).addToBackStack(null).commit();
            }
        });

        ImageButton market = (ImageButton) v.findViewById(R.id.supermarket);
        market.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new SuperMarketFragment()).addToBackStack(null).commit();
            }
        });

        ImageButton travel = (ImageButton) v.findViewById(R.id.travel);
        travel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new TravelFragment()).addToBackStack(null).commit();
            }
        });

        ImageButton shop = (ImageButton) v.findViewById(R.id.shopping);
        shop.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new ShoppingFragment()).addToBackStack(null).commit();
            }
        });

        return v;
    }

}
