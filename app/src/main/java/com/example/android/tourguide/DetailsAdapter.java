package com.example.android.tourguide;
import android.app.Activity;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;



/**
 * Created by Jacquelyn Gboyor on 10/6/2017.
 */

public class DetailsAdapter extends ArrayAdapter<Details>{

    private static final String LOG_TAG = DetailsAdapter.class.getSimpleName();

    public DetailsAdapter(Activity context, ArrayList<Details> detail) {
        // Here, we initialize the ArrayAdapter's internal storage for the context and the list.
        // the second argument is used when the ArrayAdapter is populating a single TextView.
        // Because this is a custom adapter for two TextViews and an ImageView, the adapter is not
        // going to use this second argument, so it can be any value. Here, we used 0.
        super(context, 0, detail);
    }
    String font = "fonts/Prestij_Light_Demo.otf";
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItemView = convertView;
        if(listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_items, parent, false);
        }

        // Get the {@link AndroidFlavor} object located at this position in the list
        Details currentGuide = getItem(position);
        Typeface typeface3 = Typeface.createFromAsset(getContext().getAssets(), font);
        // Find the TextView in the list_item.xml layout with the ID version_number

        ImageView picture1 = (ImageView) listItemView.findViewById(R.id.picture1);
        picture1.setImageResource(currentGuide.getPicture1());

        ImageView picture2= (ImageView) listItemView.findViewById(R.id.picture2);
        picture2.setImageResource(currentGuide.getPicture2());

        ImageView review = (ImageView) listItemView.findViewById(R.id.reviews);
        review.setImageResource(currentGuide.getReviews());

        TextView locate = (TextView) listItemView.findViewById(R.id.location_name);
        locate.setText(currentGuide.getLocation_name());
        locate.setTypeface(typeface3);

        TextView paragraph = (TextView) listItemView.findViewById(R.id.paragraph);
        paragraph.setText(currentGuide.getParagraph());
        //---------------------------------------
        ImageView amenity1 = (ImageView) listItemView.findViewById(R.id.amenity1);
        amenity1.setImageResource(currentGuide.getAmenity1());

        TextView amenity1name = (TextView) listItemView.findViewById(R.id.amenity1name);
        amenity1name.setText(currentGuide.getAmenity1name());
        //---------------------------------------
        ImageView amenity2 = (ImageView) listItemView.findViewById(R.id.amenity2);
        amenity2.setImageResource(currentGuide.getAmenity2());

        TextView amenity2name = (TextView) listItemView.findViewById(R.id.amenity2name);
        amenity2name.setText(currentGuide.getAmenity2name());
        //---------------------------------------
        ImageView amenity3 = (ImageView) listItemView.findViewById(R.id.amenity3);
        amenity3.setImageResource(currentGuide.getAmenity3());

        TextView amenity3name = (TextView) listItemView.findViewById(R.id.amenity3name);
        amenity3name.setText(currentGuide.getAmenity3name());
        //---------------------------------------
        ImageView amenity4 = (ImageView) listItemView.findViewById(R.id.amenity4);
        amenity4.setImageResource(currentGuide.getAmenity4());

        TextView amenity4name = (TextView) listItemView.findViewById(R.id.amenity4name);
        amenity4name.setText(currentGuide.getAmenity4name());
        //---------------------------------------
        ImageView amenity5 = (ImageView) listItemView.findViewById(R.id.amenity5);
        amenity5.setImageResource(currentGuide.getAmenity5());

        TextView amenity5name = (TextView) listItemView.findViewById(R.id.amenity5name);
        amenity5name.setText(currentGuide.getAmenity5name());
        //---------------------------------------
        ImageView amenity6 = (ImageView) listItemView.findViewById(R.id.amenity6);
        amenity6.setImageResource(currentGuide.getAmenity6());

        TextView amenity6name = (TextView) listItemView.findViewById(R.id.amenity6name);
        amenity6name.setText(currentGuide.getAmenity6name());
        //---------------------------------------
        ImageView amenity7 = (ImageView) listItemView.findViewById(R.id.amenity7);
        amenity7.setImageResource(currentGuide.getAmenity7());

        TextView amenity7name = (TextView) listItemView.findViewById(R.id.amenity7name);
        amenity7name.setText(currentGuide.getAmenity7name());
        //---------------------------------------
        ImageView amenity8 = (ImageView) listItemView.findViewById(R.id.amenity8);
        amenity8.setImageResource(currentGuide.getAmenity8());

        TextView amenity8name = (TextView) listItemView.findViewById(R.id.amenity8name);
        amenity8name.setText(currentGuide.getAmenity8name());
        //---------------------------------------
        ImageView amenity9 = (ImageView) listItemView.findViewById(R.id.amenity9);
        amenity9.setImageResource(currentGuide.getAmenity9());

        TextView amenity9name = (TextView) listItemView.findViewById(R.id.amenity9name);
        amenity9name.setText(currentGuide.getAmenity9name());


        // Return the whole list item layout (containing 2 TextViews and an ImageView)
        // so that it can be shown in the ListView
        return listItemView;
    }
}//End class
