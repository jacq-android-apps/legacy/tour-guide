package com.example.android.tourguide;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class MuseumsFragment extends Fragment {


    public MuseumsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.list_view, container, false);

        ArrayList<FoodDrink_Details> foodDrinkDetails = new ArrayList<FoodDrink_Details>();
        foodDrinkDetails.add(new FoodDrink_Details(R.drawable.ghibli1, R.drawable.ghibli2,R.drawable.ghibli3,
                R.drawable.ghibli4, R.drawable.ghibli5, R.drawable.ghibli_review,
                getString(R.string.museum1),getString(R.string.ghibli)));

        foodDrinkDetails.add(new FoodDrink_Details(R.drawable.art1, R.drawable.art2,R.drawable.art3,
                R.drawable.art4, R.drawable.art, R.drawable.art_review,
                getString(R.string.art_museum),getString(R.string.art)));


        FoodDrinkAdapter itemsAdapter = new FoodDrinkAdapter(getActivity(), foodDrinkDetails);
        ListView view = (ListView) v.findViewById(R.id.list);
        view.setAdapter(itemsAdapter);

        TextView Nav_Title = (TextView) v.findViewById(R.id.guideTitle);
        Nav_Title.setText(getString(R.string.museumTitle));
        Typeface title = Typeface.createFromAsset(getContext().getAssets(), getString(R.string.fonts));
        Nav_Title.setTypeface(title);


        return v;
    }

}
