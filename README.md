# 15 Incredible Things to Do in Tokyo
Android tour guide app I original created in 2017.

### 🗒️Overview

```
- 15 sites to see in Tokyo Japan
- (3 Hotels, 3 Restaurants, 2 Museums, 2 Nightclubs, 2 Shopping Centers, 2 Supermarkets, 1 Metro station)
- App contains custom objects for storing information
- Contains custom adapter
- Use of Fragments
```

### 🏁 Getting Started

#### Prerequisites

To successfully build and run this app locally in Android Studio, you need to following:

| Config       | Details                   |
|--------------|---------------------------|
| Android SDK  | 8.0 (Oreo) API 26         |
| Gradle /     | v7.3.3                    |
| $agp_version | v7.2.0                    |
| Gradle JDK   | Jetbrains runtime v17.0.7 |

In file `./build.gradle` *buildscript.repositories* & *allprojects.repositories* contain:
```
google()
mavenCentral()
```

### 📷 Screenshots

|                          Welcome                          |                        Dashboard                         |
|:---------------------------------------------------------:|:--------------------------------------------------------:|
| ![welcome page](public/screens/welcome.webp){width=300px} | ![dashboard](public/screens/dashboard.webp){width=300px} |

|                           Food & Drinks                            |                       Hotels                       |                       Museums                       |
|:------------------------------------------------------------------:|:--------------------------------------------------:|:---------------------------------------------------:|
| ![food & drinks](public/screens/food-and-drinks.webp){width=300px} | ![hotels](public/screens/hotels.webp){width=300px} | ![museums](public/screens/museum.webp){width=300px} |

|                        Nightlife                         |                           Shopping Malls                           |                          Supermarkets                          |
|:--------------------------------------------------------:|:------------------------------------------------------------------:|:--------------------------------------------------------------:|
| ![nightlife](public/screens/nightlife.webp){width=300px} | ![shopping malls](public/screens/shopping-malls.webp){width=300px} | ![supermarkets](public/screens/supermarkets.webp){width=300px} |

|                           Transportation                           | 
|:------------------------------------------------------------------:|
| ![transportation](public/screens/transportation.webp){width=300px} |

_Disclaimer: None of the images used to create this app belong to me._
